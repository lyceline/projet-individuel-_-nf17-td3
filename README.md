# Projet Individuel _ NF17 TD3

## 5. Complexe cinématographique

Un nouveau complexe cinématographique va ouvrir ses portes, et souhaite se doter d'une base de données afin de gérer le fonctionnement quotidien et surtout la gestion des films et des entrées.

### Hypothèses

- La gestion des films est au centre du système
- Nous trouvons tous les jours des séances, qui correspondent à la projection d'un film
- Pour chaque séance nous avons besoin de recenser le nombre de places occupées, et donc de places vendues
- Le complexe souhaite pouvoir proposer des abonnements à ses clients
- Il y aura aussi des ventes de boissons et de denrées alimentaires
- De plus, chaque spectateur pourra donner une note en sortant de la projection via des bornes prévues à cet usage

### Besoins

- Un film est caractérisé par un titre et une date de sortie, il possède des réalisateurs et des producteurs, et chaque film possède un genre et un âge minimum recommandé
- Un distributeur gère un ensemble de films
- Les spectateurs peuvent en sortant d'une séance noter un film (entre 0 et 5)
- Une séance de cinéma propose pour un jour et une heure la projection d'un film, en précisant la salle et le doublage (VF, VOST, etc.)
- Chaque salle possède un nombre de places
- Un vendeur vend des entrées pour une séance
- Chaque entrée est soit un ticket unitaire, soit un ticket tiré d'une carte d’abonnement
- Dans le cas d'un ticket acheté à l'unité, il faut sélectionner le tarif adéquat (adulte, étudiant, enfant, dimanche, etc.)
- Dans le cas d'un abonnement, il faut vérifier qu'il reste encore des places sur la carte par rapport au dernier rechargement de carte
- Chaque vendeur peut faire des ventes de produits (boisson ou alimentaire)
- Nous avons besoin d'obtenir différentes statistiques, comme par exemple le pourcentage d'occupation des séances, la mesure du succès d'un film, la part d'abonnés, le revenu généré par un film, le nombre de spectateurs par film, etc

