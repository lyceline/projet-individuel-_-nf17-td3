-- INSERT

INSERT INTO Distributeur VALUES
    ('Cinema Public Films'),
    ('Memento Films Distribution'),
    ('Paramount Pictures'),
    ('SND'),
    ('The Walt Disney Company'),
    ('UGC Distribution');

INSERT INTO Film VALUES
    ('Cuban Network', '2020-01-29', '["Espionnage", "Thriller"]', NULL, 127, 'Memento Films Distribution'),
    ('L Extraodinaire Voyage de Marona', '2020-01-08', '["Animation", "Drame", "Famille"]', 6, 92, 'Cinema Public Films'),
    ('Sonic Le Film', '2020-02-12', '["Comédie", "Aventure"]', 6, 99, 'Paramount Pictures'),
    ('Mine De Rien', '2020-02-26', '["Comédie"]', NULL, 85, 'UGC Distribution'),
    ('The Gentlemen','2020-02-05', '["Policier", "Action"]', NULL, 113,'SND'),
    ('L Appel de la Forêt', '2020-02-19', '["Aventure", "Famille", "Drame"]', 10, 100, 'The Walt Disney Company');

INSERT INTO Salle VALUES
    (1, 100),
    (2, 100),
    (3, 100),
    (4, 130),
    (5, 130),
    (6, 130),
    (7, 170),
    (8, 170),
    (9, 200),
    (10, 200);

INSERT INTO Seance ("titrefilm", "numerosalle", "creneau", "doublage") VALUES
    ('Cuban Network', 1, '2020-01-29 10:30:00', 'VF'),
    ('Cuban Network', 9, '2020-01-30 16:15:00', 'VOSTFR'),
    ('Cuban Network', 5, '2020-01-31 13:40:00', 'VO'),
    ('Cuban Network', 7, '2020-02-01 20:00:00', 'VF'),
    ('L Extraodinaire Voyage de Marona', 8, '2020-01-08 14:10:00', 'VF'),
    ('L Extraodinaire Voyage de Marona', 2, '2020-01-09 17:30:00', 'VF'),
    ('L Extraodinaire Voyage de Marona', 6, '2020-01-11 10:15:00', 'VF'),
    ('L Extraodinaire Voyage de Marona', 4, '2020-01-12 15:50:00', 'VF'),
    ('Sonic Le Film', 10, '2020-02-12 18:15:00', 'VOSTFR'),
    ('Sonic Le Film', 9, '2020-02-13 21:10:00', 'VO'),
    ('Sonic Le Film', 7, '2020-02-14 17:30:00', 'VOSTFR'),
    ('Sonic Le Film', 8, '2020-02-15 20:50:00', 'VF'),
    ('Mine De Rien', 3, '2020-02-26 10:30:00', 'VF'),
    ('Mine De Rien', 1, '2020-02-27 10:30:00', 'VF'),
    ('Mine De Rien', 5, '2020-02-28 10:30:00', 'VF'),
    ('Mine De Rien', 6, '2020-02-29 10:30:00', 'VF'),
    ('The Gentlemen', 8, '2020-02-05 10:30:00', 'VO'),
    ('The Gentlemen', 4, '2020-02-06 10:30:00', 'VOSTFR'),
    ('The Gentlemen', 7, '2020-02-07 10:30:00', 'VF'),
    ('The Gentlemen', 10, '2020-02-09 10:30:00', 'VOSTFR'),
    ('L Appel de la Forêt', 2, '2020-02-19 10:30:00', 'VF'),
    ('L Appel de la Forêt', 9, '2020-02-20 10:30:00', 'VOSTFR'),
    ('L Appel de la Forêt', 5, '2020-02-21 10:30:00', 'VF'),
    ('L Appel de la Forêt', 3, '2020-02-24 10:30:00', 'VO');
    
INSERT INTO Client ("age") VALUES
    (11), (36), (14), (9), (6), (27), (48), (29), (17), (67), (54);

INSERT INTO Vendeur ("nom", "prenom") VALUES
    ('Delmarquette', 'Andréa'),
    ('Guicherd', 'Benjamin'),
    ('Hamon', 'Simon'),
    ('Boucly', 'Mathilde'),
    ('Mejia', 'Diego'),
    ('Poullet-Pages', 'Félix');

INSERT INTO Produit VALUES
    (1, 3, 1.90, '[{"quantité" : "1", "produit" : "Jus de pomme 33 cL", "prix" : "1.90"}]'),
    (3, 1, 7.10, '[{"quantité" : "1", "produit" : "Pop Corn sucré Maxi", "prix" : "3.20"}, {"quantité" : "2", "produit" : "Orangina 50 cL", "prix" : "3.90"}]'),
    (5, 4, 2.35, '[{"quantité" : "1", "produit" : "Glace Magnum Maxi Choco", "prix" : "2.35"}]'),
    (6, 6, 4.65, '[{"quantité" : "1", "produit" : "Pop Corn Caramel Medium", "prix" : "2.80"}, {"quantité" : "1", "produit" : "Coca Cola 50 cL", "prix" : "1.85"}]'),
    (9, 5, 4.50, '[{"quantité" : "1", "produit" : "M&Ms Peanuts 100g", "prix" : "2.70"}, {"quantité" : "1", "produit" : "Kit Kat", "prix" : "1.80"}]'),
    (11, 2, 1.60, '[{"quantité" : "1", "produit" : "Evian 50 cL", "prix" : "1.60"}]');

INSERT INTO Realisateur ("nom", "prenom") VALUES
    ('Damian', 'Anca'),
    ('Assayas', 'Olivier'),
    ('Fowler', 'Jeff'),
    ('Xu', 'Charleene'),
    ('Royer', 'Antoine'),
    ('Samudio', 'Quentin');

INSERT INTO Producteur ("nom", "prenom") VALUES
    ('Kang', 'Laëtitia'),
    ('Maubourguet', 'Anaïs'),
    ('Preux', 'Adélie'),
    ('Traoré', 'Nil'),
    ('Quantin', 'Mathilde'),
    ('Legras', 'Lucie');

INSERT INTO Note VALUES
    (2, 'Cuban Network', 4),
    (2, 'L Appel de la Forêt', 3),
    (6, 'Mine De Rien', 1),
    (6, 'Cuban Network', 4),
    (6, 'The Gentlemen', 5),
    (7, 'Mine De Rien', 2),
    (7, 'Sonic Le Film', 3),
    (8, 'The Gentlemen', 3),
    (8, 'Mine De Rien', 4),
    (8, 'L Appel de la Forêt', 1),
    (8, 'L Extraodinaire Voyage de Marona', 3),
    (9, 'Sonic Le Film', 5),
    (9, 'L Appel de la Forêt', 4),
    (10, 'The Gentlemen', 2),
    (10, 'Cuban Network', 3),
    (10, 'Mine De Rien', 1),
    (11, 'L Extraodinaire Voyage de Marona', 3),
    (11, 'Sonic Le Film', 4),
    (11, 'Mine De Rien', 5),
    (11, 'The Gentlemen', 4);

INSERT INTO Realisation VALUES
    (1, 'L Extraodinaire Voyage de Marona'),
    (2, 'Cuban Network'),
    (3, 'Sonic Le Film'),
    (4, 'Mine De Rien'),
    (5, 'The Gentlemen'),
    (6, 'L Appel de la Forêt');

INSERT INTO Production VALUES
    (1, 'L Extraodinaire Voyage de Marona'),
    (2, 'Cuban Network'),
    (3, 'Sonic Le Film'),
    (4, 'Mine De Rien'),
    (5, 'The Gentlemen'),
    (6, 'L Appel de la Forêt');

INSERT INTO PrixTarif VALUES
    ('Adulte', 7.00),
    ('Etudiant', 5.30),
    ('Enfant', 4.50),
    ('Dimanche', 5.00);

INSERT INTO Tarif VALUES
    (1, 'Enfant'),
    (2, 'Adulte'),
    (3, 'Enfant'),
    (4, 'Enfant'),
    (5, 'Etudiant'),
    (6, 'Adulte'),
    (7, 'Dimanche'),
    (8, 'Adulte'),
    (9, 'Etudiant'),
    (10, 'Adulte'),
    (11, 'Adulte');

INSERT INTO Unitaire VALUES
    (1, 9, 2, 1),
    (2, 6, 2, 4),
    (3, 8, 4, 2),
    (4, 10, 4, 5),
    (5, 4, 6, 3),
    (6, 5, 6, 4),
    (7, 1, 8, 6),
    (8, 14, 8, 1),
    (9, 20, 10, 2),
    (10, 3, 11, 3);


INSERT INTO NomPrenomAbo VALUES
    ('andry.razafindrabe@etu.utc.fr', 'Razafindrabe', 'Andry'),
    ('simon.metivier@etu.utc.fr', 'Métivier', 'Simon'),
    ('hiba.zarougui@etu.utc.fr', 'Zarougui', 'Hiba'),
    ('hichem.taouaf@etu.utc.fr', 'Taouaf', 'Hichem'),
    ('hiba.roqdi@etu.utc.fr', 'Roqdi', 'Hiba');

INSERT INTO TarifAbo VALUES
    (1, 5.00),
    (3, 5.00),
    (5, 5.00),
    (7, 5.00),
    (9, 5.00);

INSERT INTO Abonnement VALUES
    (11, 1, 1, 1,'andry.razafindrabe@etu.utc.fr',  3, '2020-05-15'),
    (12, 2, 1, 2, 'andry.razafindrabe@etu.utc.fr', 0, '2020-05-15'),
    (13, 3, 3, 3, 'simon.metivier@etu.utc.fr', 2, '2020-05-02'),
    (14, 4, 3, 4, 'simon.metivier@etu.utc.fr', 5, '2020-05-02'),
    (15, 5, 5, 5, 'hiba.zarougui@etu.utc.fr', 3, '2020-05-10'),
    (16, 6, 5, 6, 'hiba.zarougui@etu.utc.fr', 7, '2020-05-10'),
    (17, 7, 7, 1, 'hichem.taouaf@etu.utc.fr', 1, '2020-05-29'),
    (18, 8, 7, 2, 'hichem.taouaf@etu.utc.fr', 3, '2020-05-29'),
    (19, 9, 9, 3, 'hiba.roqdi@etu.utc.fr', 2, '2020-05-07'),
    (20, 10, 9, 4, 'hiba.roqdi@etu.utc.fr', 3, '2020-05-07');




