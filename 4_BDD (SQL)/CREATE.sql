-- TYPES

CREATE TYPE typeDoublage AS ENUM ('VO', 'VF', 'VOSTFR');
CREATE TYPE typeTarif AS ENUM ('Adulte', 'Etudiant', 'Enfant', 'Dimanche');

-- CLASSES

CREATE TABLE Distributeur(
    nom VARCHAR NOT NULL,
    PRIMARY KEY(nom)
);

CREATE TABLE Film(
    titre VARCHAR NOT NULL,
    dateSortie DATE NOT NULL,
    genre JSON NOT NULL,
    ageMin INTEGER,
    duree INTEGER NOT NULL,
    nomDistrib VARCHAR NOT NULL,
    PRIMARY KEY(titre),
    FOREIGN KEY (nomDistrib) REFERENCES Distributeur(nom),
    CHECK(ageMin>0 AND duree>0)
);

CREATE TABLE Salle(
    numero SERIAL NOT NULL,
    nbPlaces INTEGER NOT NULL,
    PRIMARY KEY(numero),
    CHECK(nbPlaces>0)
);

CREATE TABLE Seance(
    id SERIAL NOT NULL,
    titreFilm VARCHAR NOT NULL,
    numeroSalle INTEGER NOT NULL,
    creneau DATE NOT NULL,
    doublage typeDoublage NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(titreFilm) REFERENCES Film(titre),
    FOREIGN KEY(numeroSalle) REFERENCES Salle(numero),
    UNIQUE(creneau, numeroSalle),
    UNIQUE(creneau, titreFilm)
);

CREATE TABLE Client(
    id SERIAL NOT NULL,
    age INTEGER NOT NULL,
    PRIMARY KEY(id),
    CHECK(age>0)
);

CREATE TABLE Vendeur(
    id SERIAL NOT NULL,
    nom VARCHAR NOT NULL,
    prenom VARCHAR NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE Produit(
    idClient INTEGER NOT NULL,
    idVendeur INTEGER NOT NULL,
    prixTotal NUMERIC(4,2) NOT NULL,
    descriptif JSON NOT NULL,
    PRIMARY KEY(idClient, idVendeur),
    FOREIGN KEY(idClient) REFERENCES Client(id),
    FOREIGN KEY(idVendeur) REFERENCES Vendeur(id),
    CHECK(prixTotal>0.00)
);

CREATE TABLE Realisateur(
    id SERIAL NOT NULL,
    nom VARCHAR NOT NULL,
    prenom VARCHAR NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE Producteur(
    id SERIAL NOT NULL,
    nom VARCHAR NOT NULL,
    prenom VARCHAR NOT NULL,
    PRIMARY KEY(id)
);

-- CLASSES D'ASSOCIATION N:M

CREATE TABLE Note(
    idClient INTEGER NOT NULL,
    titreFilm VARCHAR NOT NULL,
    note INTEGER,
    PRIMARY KEY(idClient, titreFilm),
    FOREIGN KEY(idClient) REFERENCES Client(id),
    FOREIGN KEY(titreFilm) REFERENCES Film(titre),
    CHECK(note BETWEEN 0 AND 5)
);

CREATE TABLE Realisation(
    idReal INTEGER NOT NULL,
    titreFilm VARCHAR NOT NULL,
    PRIMARY KEY(idReal, titreFilm),
    FOREIGN KEY(idReal) REFERENCES Realisateur(id),
    FOREIGN KEY(titreFilm) REFERENCES Film(titre)
);

CREATE TABLE Production(
    idProd INTEGER NOT NULL,
    titreFilm VARCHAR NOT NULL,
    PRIMARY KEY(idProd, titreFilm),
    FOREIGN KEY(idProd) REFERENCES Producteur(id),
    FOREIGN KEY(titreFilm) REFERENCES Film(titre)
);

CREATE TABLE PrixTarif(
    type typeTarif NOT NULL,
    prix NUMERIC NOT NULL,
    PRIMARY KEY(type),
    CHECK(prix>0.00)
);

CREATE TABLE Tarif(
    idClient INTEGER NOT NULL,
    tarif typeTarif NOT NULL,
    PRIMARY KEY(idClient),
    FOREIGN KEY(idClient) REFERENCES Client(id),
    FOREIGN KEY(tarif) REFERENCES PrixTarif(type)
);

CREATE TABLE Unitaire(
    idEntree INTEGER NOT NULL,
    idSeance INTEGER NOT NULL,
    idClient INTEGER NOT NULL,
    idVendeur INTEGER NOT NULL,
    PRIMARY KEY (idEntree),
    FOREIGN KEY(idSeance) REFERENCES Seance(id),
    FOREIGN KEY(idClient) REFERENCES Tarif(idClient),
    FOREIGN KEY(idVendeur) REFERENCES Vendeur(id),
    UNIQUE(idSeance, idClient)
);

CREATE TABLE NomPrenomAbo(
    email VARCHAR NOT NULL,
    nom VARCHAR NOT NULL,
    prenom VARCHAR NOT NULL,
    PRIMARY KEY(email)
);

CREATE TABLE TarifAbo(
    idClient INTEGER NOT NULL,
    prix NUMERIC(4,2) NOT NULL,
    PRIMARY KEY(idClient),
    FOREIGN KEY(idClient) REFERENCES Client(id),
    CHECK(prix>0.00)
);

CREATE TABLE Abonnement(
    idEntree INTEGER NOT NULL,
    idSeance INTEGER NOT NULL,
    idClient INTEGER NOT NULL,
    idVendeur INTEGER NOT NULL,
    email VARCHAR NOT NULL,
    nbRecharge INTEGER NOT NULL,
    dateDernierRech DATE NOT NULL,
    PRIMARY KEY (idEntree),
    FOREIGN KEY(idSeance) REFERENCES Seance(id),
    FOREIGN KEY(idClient) REFERENCES TarifAbo(idClient),
    FOREIGN KEY(idVendeur) REFERENCES Vendeur(id),
    FOREIGN KEY(email) REFERENCES NomPrenomAbo(email),
    UNIQUE(idSeance, idClient),
    CHECK(nbRecharge>=0)
);

-- VUES

    -- Contraintes de cardinalité minimale 1 dans les association 1:N
    -- (toutes ces vues doivent retourner des tableaux vides)

CREATE VIEW vFilmSeance AS
SELECT titre FROM Film
EXCEPT
SELECT titreFilm FROM Seance;

CREATE VIEW vDistributeurFilm AS
SELECT nom FROM Distributeur
EXCEPT
SELECT nomDistrib FROM Film;

    -- Contraintes de cardinalité minimale 1 dans les association N:M
    -- (toutes ces vues doivent retourner des tableaux vides)

CREATE VIEW vRealisateurRealisation AS
SELECT id FROM Realisateur
EXCEPT
SELECT idReal FROM Realisation;

CREATE VIEW vFilmRealisation AS 
SELECT titre FROM Film
EXCEPT 
SELECT titreFilm FROM Realisation;

----------

CREATE VIEW vProducteurProduction AS
SELECT id FROM Producteur
EXCEPT 
SELECT idProd FROM Production;

CREATE VIEW vFilmProduction AS 
SELECT titre FROM Film
EXCEPT
SELECT titreFilm FROM Production;

/*
-- UTILISATEURS

    -- Client

CREATE GROUP Client;
CREATE USER client00001 WITH PASSWORD '********';
CREATE USER client00002 WITH PASSWORD '********';
CREATE USER client00003 WITH PASSWORD '********';
CREATE USER client00004 WITH PASSWORD '********';
CREATE USER client00005 WITH PASSWORD '********';
GRANT client00001, client00002, client00003, client00004, client00005 TO Client;

GRANT INSERT, UPGRADE ON Client TO Client;
GRANT INSERT, DELETE, UPGRADE ON Note TO Client;
GRANT INSERT, DELETE, UPGRADE ON Abonnement TO Client;

    -- Vendeur

CREATE GROUP Vendeur;
CREATE USER vendeur00001 WITH PASSWORD '********';
CREATE USER vendeur00002 WITH PASSWORD '********';
CREATE USER vendeur00003 WITH PASSWORD '********';
CREATE USER vendeur00004 WITH PASSWORD '********';
CREATE USER vendeur00005 WITH PASSWORD '********';
CREATE USER vendeur00006 WITH PASSWORD '********';
GRANT vendeur00001, vendeur00002, vendeur00003, vendeur00004, vendeur00005, vendeur00006 TO Vendeur;

GRANT SELECT, UPGRADE ON Vendeur TO Vendeur;
GRANT ALL PRIVILEGES ON Abonnement TO Vendeur;
GRANT SELECT, INSERT, UPGRADE ON Client TO Vendeur;

    -- Informaticien

CREATE GROUP Informaticien;
CREATE USER PaulineLy WITH PASSWORD '********';
CREATE USER SebastienDam WITH PASSWORD '********';
CREATE USER CarreMaxime WITH PASSWORD '********';
CREATE USER LudovicHsu WITH PASSWORD '********';
CREATE USER HadrienPadovani WITH PASSWORD '********';
GRANT PaulineLy, SebastienDam, CarreMaxime, LudovicHsu, HadrienPadovani TO Informaticien;

GRANT ALL PRIVILEGES ON Film, Seance, Salle TO Informaticien;
GRANT ALTER ON Client, Entree, Unitaire, Abonnement, Note, Distributeur, Vendeur, Produit TO Informaticien;

    -- PDG

CREATE GROUP PDG;
CREATE USER pdg2020 WITH PASSWORD '********';
GRANT pdg2020 TO PDG;

GRANT ALL PRIVILEGES ON Film, Seance, Salle, Distributeur, Vendeur, Produit TO PDG;
GRANT SELECT, DELETE ON Client To PDG;

*/