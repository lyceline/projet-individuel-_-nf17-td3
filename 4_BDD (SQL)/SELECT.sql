-- Vues statistiques

    -- Vues pour obtenir le nombre de places occupées pour chaque séance

CREATE VIEW vOccupationSeanceUni (Film, Creneau, PlacesUniVendues) AS
SELECT Seance.titreFilm, Seance.creneau, COUNT(DISTINCT Unitaire.idEntree)
FROM Seance, Unitaire
WHERE (Unitaire.idSeance=Seance.id)
GROUP BY (Seance.id, Seance.creneau);

CREATE VIEW vOccupationSeanceAbo (Film, Creneau, PlacesAboVendues) AS
SELECT Seance.titreFilm, Seance.creneau, COUNT(DISTINCT Abonnement.idEntree)
FROM Seance, Abonnement
WHERE (Abonnement.idSeance=Seance.id)
GROUP BY (Seance.id, Seance.creneau);

CREATE VIEW vOccupationSeance (Film, Creneau, nbPlacesOccupees) AS
SELECT Seance.titreFilm, Seance.creneau, vOSU.PlacesUniVendues + vOSA.PlacesAboVendues
FROM Seance, vOccupationSeanceUni vOSU, vOccupationSeanceAbo vOSA
WHERE Seance.titreFilm=vOSU.Film AND Seance.titreFilm=vOSA.Film
AND Seance.creneau=vOSU.Creneau AND Seance.creneau=vOSA.Creneau
GROUP BY (Seance.id, Seance.creneau, vOSU.PlacesUniVendues, vOSA.PlacesAboVendues)
ORDER BY vOSU.PlacesUniVendues + vOSA.PlacesAboVendues DESC;

    -- Vue pour calculer le succès d'un film en utilisant les notes données par les spectateurs

CREATE VIEW vSuccesFilm (Film, MoyenneNotes) AS
SELECT titreFilm, ROUND(AVG(N.note), 2) AS MoyenneNotes
FROM Note N
GROUP BY (titreFilm)
ORDER BY MoyenneNotes DESC;

    -- Vue pour calculer le taux d'abonnés parmi tous les clients

CREATE VIEW vPartAbo (nbAbonnes, nbClients, tauxAbo) AS 
SELECT
  (SELECT COUNT(DISTINCT idClient) FROM Abonnement),
  (SELECT COUNT(DISTINCT id) FROM Client),
  CONCAT(ROUND(((SELECT COUNT(DISTINCT idClient) FROM Abonnement)*1.0/COUNT(DISTINCT Client.id)*1.0)*100, 2), ' %')
FROM Abonnement, Client;

    -- Vue pour obtenir les films par genre

CREATE VIEW vGenreFilms (Genre, Film) AS
SELECT genre.*, Film.titre
FROM Film, JSON_ARRAY_ELEMENTS_TEXT(Film.genre) genre
GROUP BY genre.value, Film.titre
ORDER BY genre.value DESC;
