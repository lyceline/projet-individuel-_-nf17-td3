# Note De Clarification

## Contexte du projet

Dans le cadre de l'UV NF17, je suis amenée à gérer le fonctionnement quotidien, et principalement, la gestion des films et des entrées d'un nouveau complexe cinématographique.

## Acteurs du projet

*  **Maître d'ouvrage** : Alessandro Victorino
*  **Maître d'oeuvre** : Céline Ly

## Objets et contraintes associées

#### Le package **ProjectionFilm** est constitué des tables **Film**, **Seance**, et **Salle** :

##### Film
* *titreFilm* : Nous supposons que les titres de films seront uniques, et qu'ils feront office de **clé**.
* *dateSortie* : Cette date devrait être situé dans le **passé**.
* *idReal* : Il peut y avoir plusieurs réalisateurs pour un film. Il s'agit d'une **clé étrangère** vers la table *Realisateur*.
* *idProd* : Il peut y avoir plusieurs producteurs pour un film. Il s'agit d'une **clé étrangère** vers la table *Producteur*.
* *genreFilm* : Sera implémenté comme un attribut **JSON** multivalué.
* *ageMin* : Doit être un entier **positif**.
* *dureeFilm* : Indique la durée du film, afin de savoir quand une séance se termine, et donc quand une salle se libèrera, se note en minutes et doit être positive.
* *nomDistrib* : Un distributeur s'occupe d'un ensemble de film. Il s'agit d'une **clé étrangère** vers la table *Distributeur*.

##### Seance
* *idSeance* : **Clé** de la table *Seance*.
* *titreFilm* : Correspond au film en question, projeté à cette séance. Il s'agit d'une **clé étrangère** vers la table *Film*.
* *numeroSalle* : Correspond à la salle où se déroule la séance. Il s'agit d'une **clé étrangère** vers la table *Salle*.
* *creneau* : La date et l'heure de la séance.
* *doublage* : Une énumération {VO, VF, VOSTFR} (il peut y avoir d'autres possibilités de doublage encore, à voir...).

##### Salle
* *numeroSalle* : Les salles seront supposément notées dans l'ordre croissant à partir de 1. Chaque numéro de salle est unique et constitue la **clé** de la table *Salle*.
* *nbPlaces* : Ce nombre devra être **positif**.

#### Le package **ServiceClient** est constitué des tables **Client**, **Note**, **Entree**, **Unitaire**, **Abonnement**, **Vendeur**, et **Produit** :

##### Client
* *idClient* : **Clé** de la table *Client*. Identifiant quelconque, qui changera à chaque nouvel achat de n'importe quel client. Si un client achète une entrée un jour A, puis revient acheter une autre entrée le jour B, son ID sera différent. Cependant, s'il est abonné, nous pourrons voir tous les achats que ce client a effectué en récupérant l'ID de son abonnement (cf. attribut suivant).
* *age* : Correspond à l'âge du client, doit donc être **positif**.

##### Note
* *idClient* : Il s'agit d'une **clé étrangère** vers la table *Client*, correspondant à l'ID du client qui a donné cette note.
* *titreFilm* : Il s'agit d'une **clé étrangère** vers la table *Film*, correspondant au Film pour lequel cette note a été attribuée.
* *note* : C'est la note attribuée par le client après avoir assisté à la projection de ce film. Elle doit être comprise **entre 0 et 5**.

##### Nous avons ici un héritage de la table **Entree** dans le sous-package **Entree** :

##### Entree (abstraite)
* *idEntree* : **Clé** à la table *Entree* de type serial, permettant ainsi de comptabiliser le nombre d'entrées vendues.
* *idSeance* : Il s'agit d'une **clé étrangère** vers la table *Seance*, correspondant à la séance de l'entrée achetée.
* *idClient* : ID du client qui a acheté cette entrée. Il s'agit d'une **clé étrangère** vers la table *Client*.
* *idVendeur* : ID du vendeur qui a vendu cette entrée (=0 si l'entrée ne provient pas d'un vendeur). Il s'agit d'une **clé étrangère** vers la table *Vendeur*.
* *prix* : Ce prix dépendra du ticket acheté, selon qu'il soit un ticket venant de la recharge d'une carte d'abonnement ou un ticket unitaire, où le tarif variera en fonction de l'âge (Enfant, Adulte, Senior) du client, le statut (Etudiant ou non) du client, ou le jour (Dimanche ou non) de la séance, etc...(il y a sûrement encore beaucoup de type de tarif mais nous restrons sur ce modèle simplifié).

##### **Unitaire** hérite de **Entree**

##### Unitaire
* *typeTarif* : Énumération {Senior, Adulte, Etudiant, Enfant, Groupe, Dimanche}

##### **Abonnement** hérite de **Entree**

##### Abonnement
* *nom* : Nom du client abonné.
* *prenom* : Prénom du client abonné.
* *email* : Email du client abonné.
* *nbRecharge* : Nombre de tickets rechargés restant dans la carte d'abonnement.
* *dateDernierRechargement* : Date du dernier rechargement (utile ou pas?).

##### Vendeur
* *id* : **Clé** de la table *Vendeur*.
* *nom* : Nom du vendeur.
* *prenom* : Prénom du vendeur.

##### Produit
* *idClient* : ID du client qui a acheté ce produit. Il s'agit d'une **clé étrangère** vers la table *Client*.
* *idVendeur* : ID du vendeur qui a vendu ce produit (=0 si l'entrée ne provient pas d'un vendeur). Il s'agit d'une **clé étrangère** vers la table *Vendeur*.
* *prixTotal* : Prix total des produits.
* *descriptif* : Descriptif du produit acheté (quantité, nom du produit, prix), implémenté comme un attribut JSON multivalué, composé des clés "nom", "quantité", et "prix".

#### Autres objets

##### Distributeur
* *nom* : Nom du distributeur d'un groupe de film/de la compagnie distributrice, et **clé** de la table *Distributeur*.

##### Realisateur
* *idReal* : ID du réalisateur, et **clé** de la table *Réalisateur*.
* *nom* : Nom du réalisateur.
* *prenom* : Prénom du réalisteur.

##### Producteur
* *idProd* : ID du producteur, et **clé** de la table *Producteur*.
* *nom* : Nom du producteur.
* *prenom* : Prénom du producteur.

## Choix d'associations

#### Association 1:N
* **Film 1--N Seance** : Pour un film, il existe un certain nombre de séances dans lesquelles ce film est projeté, et une séance correspond à la projection d'un seul film.
* **Salle 1--N Seance** : Une salle du complexe cinématographique peut se voir attribuer plusieurs séances de projection, et une séance se déroule dans une seule salle en particulier.
* **Client 1--N Entree** : Un client peut acheter plusieurs entrées, mais une entrée est associée à un seul client.
* **Client 1--N Produit** : Un client peut acheter plusieurs produits, mais un produit est associée à un seul client.
* **Distributeur 1--N Film** : Un distributeur gère un ensemble de films.
* **Vendeur 1--N Entree** : Un vendeur vend des entrée, mais une entrée est vendue par un seul vendeur.
* **Vendeur 1--N Produit** : Un vendeur peut vendre plusieurs produits.

#### Association N:M
* **Client N--M Film** : Un client peut regarder (et noter) plusieurs film, et un film peut être noté par plusieurs clients.
    * Classe d'association Note : Un client peut noter un film après l'avoir regardé.
* **Realisateur N--M Film** : Un réalisteur réalise un certain nombre de films, et un film peut être réalisé par plusieurs réalisateurs.
* **Producteur N--M Film** : Un producteur produit un certain nombre de films, et un film peut être produit par plusieurs producteur.

#### Composition
Nous avons une seule composition dans notre modélisation, qui est la composition entre **Seance** et **Entree**. Nous considérons qu'une séance est constituée d'entrées, et qu'une entrée "fait partie" d'une séance. Une entrée ne peut effectivement pas exister si la séance n'existe pas. Si on supprime la séance, toutes les entrées qui lui sont associées seront supprimées aussi.

#### Choix d'héritage
Nous avons un seul héritage dans notre modélisation, qui est l'héritage de **Entree** par **Unitaire** et **Abonnement**.
1. La classe mère **Entree** est *abstraite*.
2. L'héritage est *exclusif*, c'est-à-dire que aucun objet de la classe **Unitaire** n'appartient à la classe **Abonnement**, et une **Entree** ne peut être que soit un **Unitaire**, soit un **Abonnement**.
3. L'héritage est **presque complet**, c'est-à-dire que les classes filles ont des attributs propres mais aucune associations propres.
4. La classe mère **Entree** possède des *associations sortantes* (c'est elle qui référence mais elle n'est jamais référencée).

Nous choisissons alors un **héritage par classse filles** afin d'éviter les redondances ou les valeurs nulles systématiques, et les contraintes complexes dûes à la classe mère abstraite ou l'héritage exclusif.

## Utilisateurs

Le **PDG** est le directeur de l'entreprise. Nous supposons qu'il a la possibilité d'insérer ou supprimer des informations de presque tous les objets du complexe cinématographique (ex : Film, Produit, etc.).

L'**Informaticien** gère et coordine les séances, les films projeté et attribue les salles aux séances. Nous supposons alors qu'il peut mettre à jour les données des tables Film et Seance, mais aussi modifier la structure de presque tous les objets/tables du complexe (ex : il pourrait modifier la structure de la table Abonnement en ajoutant des attributs tels que numeroTelephone, si demande du PDG).

Le **Vendeur** est celui qui s'occupe de vendre les tickets d'entrée et les produits aux clients. Il peut donc avoir accès à la plupart des informations concernant le client (savoir s'il est abonné, etc.), les tickets d'entrée (avoir les tarifs, voir les séances d'un certain film, savoir s'il reste des places à telle séance, etc.), et les produits (avoir le prix des produits, etc.).

Le **Client** est le spectateur qui vient assister à la projection d'un film. Il peut avoir accès et même modifier, supprimer ou ajouter des données et informations sur son abonnement. Il a aussi accès aux tables Film et Seance pour voir les film en cours de projection et acheter son entrée.

|**Gestion des droits**| **Film** | **Seance** | **Salle** | **Client** | **Entree** | **Unitaire** | **Abonnement** | **Note** | **Distributeur** | **Vendeur** | **Produit** | **Realisateur** | **Producteur** |
|-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| **PDG** | SELECT INSERT DELETE | SELECT INSERT DELETE UPGRADE | SELECT INSERT DELETE | SELECT DELETE | SELECT | SELECT | SELECT INSERT DELETE | SELECT | SELECT INSERT DELETE | SELECT INSERT DELETE | SELECT INSERT DELETE UPGRADE | SELECT INSERT DELETE UPGRADE | SELECT INSERT DELETE UPGRADE |
| **Informaticien** | SELECT UPGRADE ALTER | SELECT UPGRADE ALTER | SELECT ALTER | ALTER | ALTER | ALTER | ALTER | ALTER | ALTER | ALTER | ALTER | SELECT ALTER | SELECT ALTER |
| **Vendeur** | SELECT | SELECT | SELECT | SELECT INSERT UPGRADE | INSERT DELETE | INSERT DELETE | SELECT INSERT DELETE UPGRADE | X | X | SELECT UPGRADE | SELECT | SELECT | SELECT |
| **Client** | SELECT | SELECT | X | INSERT UPGRADE | UPGRADE | X | DELETE UPGRADE | INSERT DELETE UPGRADE | X | X | SELECT | SELECT | SELECT |

## Vues

**vOccupationSeance** : Donne le pourçentage d'occupation d'une séance. Récupère le nombre total de places vendues pour la séance en faisant une restriction sur le tableau "Entree" en prenant seulement les tuples où l'id de la séance correspond. Nous pouvons alors diviser ce nombre par le nombre de place total dans la salle te ainsi obtenir un pourçentage d'occupation.

**vSuccesFilm** : Mesure le succès d'un film. Récupère l'ensemble des notes données par les spectateurs à ce film pour en calculer une moyenne. Pour cela, nous ferons une restriction sur le tableau "Note" en prenant seulement les tuples où l'id de la séance correspond à une séance du film en question.

**vPartAbo** : Calcule la part d'abonnés parmi les clients. Compte le nombre de clients ayant un abonnement, pour ensuite le diviser par le nombre total de clients au complexe cinématographique, et ainsi obtenir la part d'abonnés.

**vRevenuGenere** : Calcule le revenu total généré par un film. Récupère le tarif de chaque entrée vendue (en considérant le cas où l'entrée provient d'une carte d'abonnement, et le cas où il s'agit d'un ticket unitaire, où le tarif varierait selon l'âge ou le statut du client, le jour de la séance, etc.). Cela est possible en récupérant les différents types de ticket unitaire et le nombre d'entrées d'abonnés achetés.

**vNbSpectateur** : Permet d'obtenir le nombre total de spectateurs d'un film. Récupère nombre de places vendues à chaque séance de ce film, de la même manière que dans **vOccupationSeance**.
