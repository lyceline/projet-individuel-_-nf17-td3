# Modèle Logique de Données

## Implémentation des classes

#### Package ProjectionFilm

- **Film**(#titreFilm : varchar, dateSortie : date, genreFilm : **JSON**, ageMin : integer, dureeFilm : integer, nomDistrib => Distributeur)

    *Avec tous les attributs **NOT NULL** sauf ageMin, dureeFilm>0 en minutes, et ageMin > 0 (ageMin est **NULL** s'il n'y a pas de limite d'âge pour le film).*
    
- **Seance**(#idSeance : serial, titreFilm => Film, numeroSalle => Salle, creneau : datetime, doublage : enum{VO, VF, VOSTFR})

    *Avec tous les attributs **NOT NULL**, (creneau, numeroSalle) **UNIQUE**, (creneau, titreFilm) **UNIQUE**.*
    
- **Salle**(#numeroSalle : serial, nbPlaces : integer)

    *Avec tous les attributs **NOT NULL** et nbPlaces > 0.*

#### Package ServiceClient

- **Client**(#idClient : serial, age : integer)

    *Avec tous les attributs **NOT NULL** et age > 0.*
    
- **Vendeur**(#idVendeur : serial, nom : varchar, prenom : varchar)

    *Avec tous les attributs **NOT NULL**.*
    
- **Produit**(#idClient => Client, #idVendeur => Vendeur, prixTotal : numeric, #descriptif : **JSON**)

    *Avec tous les attibuts **NOT NULL** et prix > 0.00.*

#### Classes d'association N:M
    
- **Note**(#idClient => Client, #titreFilm => Film, note : integer)

    *Avec tous les attributs **NOT NULL** et 5 > note > 0.*

- **Realisation**(#idReal => Realisateur, #titreFilm => Film)

- **Production**(#idProd => Producteur, #titreFilm => Film)

#### Autres classes

- **Distributeur**(#nomDistrib : varchar)
    
- **Realisateur**(#idReal : serial, nom : varchar, prenom : varchar)

    *Avec tous les attributs **NOT NULL**.*
    
- **Producteur**(#idProd : serial, nom : varchar, prenom : varchar)

    *Avec tous les attributs **NOT NULL**.*

## Contraintes de cardinalité minimale 1 dans les association 1:N (non nullité)

**Salle (1..1)--(0..N) Seance**

    Seance.numSalle NOT NULL

**Client (1..1)--(0..N) Entree**

    Entree.idClient NOT NULL

**Client (1..1)--(0..N) Produit**

    Produit.idClient NOT NULL

**Vendeur (1..1)--(0..N) Entree**

    Entree.idVendeur NOT NULL

**Vendeur (1..1)--(0..N) Produit**

    Produit.idVendeur NOT NULL

**Seance (1..1)--(0..N) Entree** : La cardinalité de la composition entre Seance et Entree est équivalente à celle d'une association 1:N

    Entree.idSeance NOT NULL

## Héritage par classe fille

- **Unitaire**(#idEntree : integer, idSeance => Seance, idClient => Client, idVendeur => Vendeur, prix : numeric, typeTarif : enum{Adulte, Etudiant, Enfant, Dimanche})

    *Avec tous les attributs **NOT NULL**, (idSeance, idClient) **UNIQUE**, et prix > 0.00.*

- **Abonnement**(#idEntree : integer, idSeance => Seance, idClient => Client, idVendeur => Vendeur, prix : numeric, nom : varchar, prenom : varchar, email : varchar, nbRecharge : integer, dateDernierRech : date)

    *Avec tous les attributs **NOT NULL**, (idSeance, idClient) **UNIQUE**, prix > 0.00, nbRecharge positif, et dateDernierRech dans le passé.*

## Normalisation

**Toutes les relations sont 3NF sauf *Unitaire* et *Abonnement* qui sont toutes les deux 1NF.**

**1. En effet, nous avons les DFE suivantes pour Unitaire :**
- idClient → typeTarif
- typeTarif → prix

Normalisons maintenant la première DFE **idClient → typeTarif** :
- **Unitaire**(#idEntree : integer, idSeance => Seance, idClient => TypeTarif, idVendeur => Vendeur)

    *Avec tous les attributs **NOT NULL** et (idSeance, idClient) **UNIQUE**.*
    
- **TypeTarif**(#idClient => Client, typeTarif : enum{Adulte, Etudiant, Enfant, Dimanche}, prix : numeric)

    *Avec tous les attributs **NOT NULL** et prix > 0.00.*
    
La relation est maintenant 2NF, il nous reste à normaliser la deuxième DFE **typeTarif → prix** :
- **Unitaire**(#idEntree : integer, idSeance => Seance, idClient => TypeTarif, idVendeur => Vendeur)

    *Avec tous les attributs **NOT NULL** et (idSeance, idClient) **UNIQUE**.*

- **TypeTarif**(#idClient => Client, typeTarif => PrixTarif)

- **PrixTarif**(#typeTarif : enum{Adulte, Etudiant, Enfant, Dimanche}, prix : numeric)

    *Avec tous les attributs **NOT NULL** et prix > 0.00.*

**2. Et les DFE suivantes pour Abonnement :**
- idClient → prix
- email → nom, prenom

Normalisons maintenant la première DFE **idClient → prix** :
- **Abonnement**(#idEntree : integer, idSeance => Seance, idClient => PrixEntreeAbo, idVendeur => Vendeur, nom : varchar, prenom : varchar, email : varchar, nbRecharge : integer, dateDernierRech : date)

    *Avec tous les attributs **NOT NULL**, (idSeance, idClient) **UNIQUE**, nbRecharge positif, et dateDernierRech dans le passé.*

- **PrixEntreeAbo**(#idClient => Client, prix : numeric)

    *Avec tous les attributs **NOT NULL** et prix > 0.00.*

La relation est maintenant 2NF, il nous reste à normaliser la deuxième DFE **email → nom, prenom** :
- **Abonnement**(#idEntree : integer, idSeance => Seance, idClient => PrixEntreeAbo, idVendeur => Vendeur, email => NomPrenomAbo, nbRecharge : integer, dateDernierRech : date)

    *Avec tous les attributs **NOT NULL**, (idSeance, idClient) **UNIQUE**, nbRecharge positif, et dateDernierRech dans le passé.*

- **PrixEntreeAbo**(#idClient => Client, prix : numeric)

    *Avec tous les attributs **NOT NULL** et prix > 0.00.*

- **NomPrenomAbo**(#email : varchar, nom : varchar, prenom : varchar)

    *Avec tous les attributs **NOT NULL**.*

## Implémentation des vues

### Contraintes de cardinalité minimale 1 dans les association 1:N

**Film (1..1)--(1..N) Seance**

    Seance.titreFilm NOT NULL AND
    PROJECTION(Film, titreFilm) = PROJECTION(Seance, titreFilm)

**Distributeur (1..1)--(1..N) Film**

    Film.nomDistrib NOT NULL AND
    PROJECTION(Distributeur, nomDistrib) = PROJECTION(Film, nomDistrib)

### Contraintes de cardinalité minimale 1 dans les association N:M

**Realisation (1..N)--(1..M) Film**

    PROJECTION(Realisateur, idReal) = PROJECTION(Realisation, idReal) AND
    PROJECTION(Realisation, titreFilm) = PROJECTION(Film, titreFilm)

**Production (1..N)--(1..M) Film**

    PROJECTION(Producteur, idProd) = PROJECTION(Production, idProd) AND
    PROJECTION(Production, titreFilm) = PROJECTION(Film, titreFilm)

### Héritage par classe fille

    INTERSECTION (PROJECTION(Unitaire,idEntree), PROJECTION(Abonnement,idEntree)) = {}
    Équivalent à : JOINTURE(Unitaire, Abonnement, Unitaire.idEntree = Abonnement.idEntree) = {}

